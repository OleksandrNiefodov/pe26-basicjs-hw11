/*
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?

- подія, це сигнал що щось відбувається від браузера.
використовується щоб виконати якусь дію після сигналу події, тобто можна відреагувати на дії юзера.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

- є такі варіанти:
 - click;
 - dbclick.
 - mousemove;
 - mouseup/mousedown;
 - mouseover/mouseout.
 
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

- це подія коли юзер натискає праву кнопку миші. Використовується для відображення контекстного меню, яке містить список дій, які юзер може виконати з елементом.

Практичні завдання
1. Додати новий абзац по кліку на кнопку:
По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
*/

// я зробив задачу двома шляхами, перший варіант - зробив окрему функцію для створення параграфу 

const sectionContent = document.querySelector('#content');

const buttonNewP = document.querySelector('#btn-click');

//  Варіант 1
const createNewParagraph = (paragraph) => {
    const newParagraph = document.createElement('p');
    newParagraph.innerText = paragraph;
    sectionContent.append(newParagraph);
    newParagraph.style.marginLeft = '50px';
    return newParagraph; 
};

buttonNewP.addEventListener('click', () => {
    createNewParagraph('New paragraph')
});

// Варіант 2
// buttonNewP.addEventListener('click', () => {
//     const newParagraph = document.createElement('p');
//     newParagraph.innerText = 'New paragraph';
//     sectionContent.append(newParagraph);
//     newParagraph.style.marginLeft = '50px';
// });

/*
2. Додати новий елемент форми із атрибутами:
Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
*/

const form = document.querySelector('#form-section');

const footer = document.querySelector('footer');
footer.style.margin = '50px';

const createNewInput = document.createElement('button');
createNewInput.id = 'btn-input-create';
createNewInput.innerText = 'Create Input';
createNewInput.style.padding = '20px'; 
createNewInput.style.marginLeft = '50px';
form.append(createNewInput);

createNewInput.addEventListener('click', () => {
    const newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.placeholder = 'Enter text';
    newInput.name = 'input';
    newInput.style.margin = '20px', '50px';
    newInput.style.display = 'block';
    footer.before(newInput);
});